;;; evil-integration.el --- Evil part of the Emacs config -*- lexical-binding: t -*-
;;; Commentary:
;;; Personal additions to the Emacs Config

;; Author: Abraham Raji <avronr@tuta.io>
;; Version: 1.0.0
;; Keywords: Config, Minimal Lisp
;; License: GPL v3

;;; Code:
(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))
(use-package evil-leader
    :ensure t
    :config
    (global-evil-leader-mode))
(evil-leader/set-leader "<SPC>")
(evil-leader/set-key
   "f f" 'counsel-find-file
   "f s" 'save-buffer
   "g g" 'magit-status
   "i d" 'insert-current-date
   "1" 'delete-other-windows
   "o s t" 'org-insert-structure-template
   "b" 'switch-to-buffer
   "k" 'kill-current-buffer)
(use-package cl-lib
  :ensure t)
(use-package evil-collection
  :after evil
  :ensure t
  :custom (evil-collection-use-company-tng nil)
  :config
  (evil-collection-init))

(provide 'evil-integration)
;;; evil-integration.el ends here
